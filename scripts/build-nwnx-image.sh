#!/bin/bash

usage() { echo "$0 usage:" && grep " .)\ #" $0; exit 0; }

DOCKERFILE=Dockerfile.nwnx

while getopts ":hs:z:" o; do
    case "${o}" in
        s) # Optional suffix for the image tag - eg .1
            SUFFIX=${OPTARG}
            ;;
        z) # Location of the NWNX Libraries Zip File eg ./Binaries/NWNX-EE.zip or https://21-117715326-gh.circle-artifacts.com/0/root/project/Binaries/NWNX-EE.zip
            NWNX_ZIP=${OPTARG}
            ;;
        h | *) # Display help
            usage
            exit 0
            ;;
    esac
done
shift $((OPTIND-1))

if [[ ( -z ${NWNX_ZIP} ) ]]; then
    echo ""
    echo "Required arguments -z <location of NWNX Library Zip File>"
    usage
    echo ""
fi

docker build -t efu_ee/nwserver:8179${SUFFIX} --build-arg NWNX_ZIP=${NWNX_ZIP} . -f ${DOCKERFILE}
